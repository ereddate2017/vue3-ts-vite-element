# vue3TsViteElement

#### 介绍
Vue3(vuex，vue-router，vue-i18n)+TypeScript+Vite+ElementUI，全新后台基础模板，支持dev,testing,release,prod多个环境编译打包和本地开发测试

#### 软件架构
Vue3(vuex，vue-router，vue-i18n)+TypeScript+Vite+ElementUI

