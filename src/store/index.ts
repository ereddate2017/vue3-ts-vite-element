import { createStore } from "vuex";
import menuModel from "./modules/menu/index";
const store = createStore({
  modules: {
    menuModel,
  },
  state() {
    return {
      siteName: "管理平台",
      menuCollapse: false,
      token: "",
      system: {
        userInfo: { roles: ["admin"] },
      },
    };
  },
  mutations: {
    SETMENUCOLLAPSE(state, v: boolean) {
      state.menuCollapse = v;
      window.localStorage.setItem("menuCollapse", JSON.stringify(v));
    },
    SETTOKEN(state, v: string) {
      state.token = v;
      window.localStorage.setItem("Token", JSON.stringify(v));
    },
    REMOVETOKEN(state) {
      state.token = "";
      window.localStorage.removeItem("Token");
    },
  },
  getters: {
    token: (state) => (state.token ? state.token : ""),
    roles: (state) =>
      state.system.userInfo ? state.system.userInfo.roles : [],
  },
  actions: {
    getMenuCollapse(store) {
      return new Promise((resolve, reject) => {
        try {
          let collapse = JSON.parse(
            window.localStorage.getItem("menuCollapse") || "false"
          );
          store.commit("SETMENUCOLLAPSE", collapse);
          resolve(collapse);
        } catch (err) {
          reject(err);
        }
      });
    },
    setMenuCollapse(store, data) {
      return new Promise((resolve, reject) => {
        store.commit("SETMENUCOLLAPSE", data);
        resolve({});
      });
    },
    submitLoginForm(store, data) {
      return new Promise((resolve, reject) => {
        store.commit("SETTOKEN", "111111");
        resolve({});
      });
    },
    quitLogin(store) {
      return new Promise((resolve, reject) => {
        store.commit("REMOVETOKEN");
        resolve({});
      });
    },
  },
});
export default store;
