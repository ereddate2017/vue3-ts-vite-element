import request from "@/utils/request";
export default {
  actions: {
    getMenuListData(store: any) {
      return new Promise((resolve, reject) => {
        /*  request
          .get("/")
          .then((res) => { */
        resolve({
          menus: [
            {
              create_time: "2021-10-11 14:13:38",
              delete_time: 0,
              desc: "",
              icon: "el-icon-house",
              id: 43,
              name: "microApp",
              order: 1,
              parent_id: 0,
              path: "",
              route: "",
              status: 1,
              type: 1,
              update_time: "2022-01-11 18:39:37",
              children: [
                {
                  create_time: "2021-10-11 14:15:14",
                  delete_time: 0,
                  desc: "",
                  icon: "el-icon-s-platform",
                  id: 44,
                  name: "工作台",
                  order: 55,
                  parent_id: 43,
                  path: "/order",
                  route: "/",
                  status: 1,
                  type: 2,
                  update_time: "2022-01-11 14:15:43",
                },
              ],
            },
          ],
        });
        /* })
          .catch((err) => reject(err)); */
      });
    },
  },
};
