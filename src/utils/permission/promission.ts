import store from "@/store";

function checkPermission(el: any, binding: any) {
  const { value } = binding;
  const roles = store.getters && store.getters.roles;
  //console.log(value, roles, JSON.stringify(store.state.system));
  if (value && value instanceof Array) {
    if (value.length > 0) {
      const permissionRoles = value;

      const hasPermission = roles.some((role: any) => {
        return permissionRoles.includes(role);
      });

      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el);
      }
    }
  } else {
    console.error(`need roles! Like v-permission="['admin','editor']"`);
  }
}

export default {
  mounted(el: any, binding: any) {
    checkPermission(el, binding);
  },
  updated(el: any, binding: any) {
    checkPermission(el, binding);
  },
};
