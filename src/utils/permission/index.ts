import permission from "./promission";

const install = function (app: any) {
  app.directive("permission", permission);
};

/* if (window.Vue) {
  window["permission"] = permission;
  Vue.use(install); // eslint-disable-line
} */

export default { install };
