export default {
  Home: "首页",
  userName: {
    value: "账户",
    inputPlaceHolder: "请输入帐户（邮箱地址）",
    errMsgNull: "输入账户不能为空",
    errMsgFormat: "账户名非邮箱地址，请正确输入",
  },
  password: {
    value: "密码",
    inputPlaceHolder: "请输入密码（8位）",
    errMsgNull: "输入密码不能为空",
    errMsgFormat: "密码长度不能小于8位",
  },
  forgotPassword: {
    value: "找回密码",
    tips: "请联系管理，找回密码",
  },
  login: {
    button: "登录",
    title: "用户登录",
  },
  resetForm: "重置",
  backText: "后退",
  search: "搜索",
  quitLogin: {
    title: "退出登录",
    tips: "真的要退出登录?",
  },
  msgTitle: "消息",
  userInfoTitle: "用户信息",
  editPassTitle: "修改密码",
  leftMenuTitle: "菜单列表",
};
