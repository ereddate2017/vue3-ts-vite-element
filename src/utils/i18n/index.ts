import en from "@/utils/i18n/lang/en";
import cn from "@/utils/i18n/lang/cn";
export default {
  en,
  cn,
};
