import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";
import ElementPlus from "unplugin-element-plus/vite";
import eslintPlugin from "vite-plugin-eslint";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), ElementPlus() /* , eslintPlugin() */],
  // 使用相对地址
  base: "./",
  envDir: "./viteEnv",
  envPrefix: ["VITE", "VENUS"],
  resolve: {
    // 设置@符号全局路径
    alias: {
      "@": resolve(__dirname, "./src"),
    },
  },
  server: {
    // 跨域配置
    proxy: {
      "/api": {
        target: "http://localhose:8033", // 接口域名
        changeOrigin: true,
        rewrite: (path: string) => path.replace(/^\/api/, ""),
      },
    },
    hmr: {
      overlay: false,
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        // 配置全局样式
        additionalData: `@import "./src/assets/style/theme.scss";`,
      },
    },
  },
});
